# Tapps 

Tapps (or Tesla Apps) is a dashboard for Tesla Model 3/Y browser.

![Dashboard](https://gitlab.com/MrDIYca/tapps/-/raw/master/public/img/preview.png)


# Usage

To start using the dashbaord, go to the 'Browser' on your Model 3/Y screen, then visit [the bashboard URL](https://tesla.mrdiy.ca).

# Thanks

This project was inspired by abettertheater.com. Many thanks to all the authors and contributors to the open source libraries and community.

# Note

This was originally created by https://gitlab.com/mrdiy im editing for my use all credit to mrdiy
